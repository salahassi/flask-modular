from project import app
from flask import render_template, request
from gModels.bModel import db
from .models.texts import Texts

import time

@app.route("/", methods=['GET','POST'])
def home():
    if request.method == "POST":
        new_text = Texts(request.form['text'],request.form['lat'], 
                         request.form['lon'], request.remote_addr, 
                         int(round(time.time())))
        db.session.add(new_text)
        db.session.commit()
    
    texts = Texts.query.order_by("id desc").all()
    
    return render_template("home.html", texts=texts)
