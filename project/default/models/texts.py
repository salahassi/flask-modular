from gModels.bModel import db

class Texts(db.Model):
    
    id = db.Column(db.Integer, primary_key=True)
    txt = db.Column(db.Text)
    lat = db.Column(db.Float)
    lon = db.Column(db.Float)
    ip = db.Column(db.String(30))
    date_ = db.Column(db.Integer)
    
    def __init__(self, txt, lat, lon, ip, date_):
        self.txt = txt
        self.lat = lat
        self.lon = lon
        self.ip = ip
        self.date_ = date_
        
    def __repr__(self):
        return '<Texts {txt: %r, ip: %r, lat: %r, lon: %r, date_: %r}>' % \
               (self.txt, self.ip, self.lat, self.lon, self.date_)