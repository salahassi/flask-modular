from project import app
from flask import render_template

@app.route("/users/login")
def login():
    return render_template("login.html")

