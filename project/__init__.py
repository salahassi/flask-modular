import project

import pkgutil

import config
import jinja2

from flask import Flask

app = Flask(__name__, static_folder="../static")
app.config.from_object('config.config')

modules = []
for importer, modname, ispkg in pkgutil.iter_modules(project.__path__, project.__name__ + "."):
    __import__(modname)
    modules.append(modname.split(".")[-1])

temp_dirs = [project.__path__[0] + "/" + template_path + "/templates" for template_path in modules]

temp_dirs.append(config._basedir + "/templates")

my_loader = jinja2.ChoiceLoader([
    app.jinja_loader,
    jinja2.FileSystemLoader(temp_dirs)
])

app.jinja_loader = my_loader