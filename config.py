import os

#DEV: Development, PROD: Production, TEST: Testing
ENVIRONMENT = "DEV"

_basedir = os.path.abspath(os.path.dirname(__file__))


class Default():
	PORT = 80
	DEBUG = False
	SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:root@localhost/flask'


class Development(Default):
	PORT = 8080
	DEBUG = True


class Testing(Default):
    PORT = 8080
    DEBUG = True
    TESTING = True


class Production(Default):
    pass

ENV_CONFIG = {
    "DEV": Development,
    "PROD": Production,
    "TEST": Testing
}

try:
    config = ENV_CONFIG[ENVIRONMENT]
except:
    config = Default
